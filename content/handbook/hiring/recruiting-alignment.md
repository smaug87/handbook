---
title: "Talent Acquisition Alignment"
description: "This page is an overview of the search team alignment and the talent acquisition platform directly responsible individual in talent acquisition operations and talent brand."
---

## Search Team Alignment by Department

| Department                    | Recruiter   | Candidate Experience Specialist    |
|--------------------------|-----------------|-------------------------------------|
| Executive          | Rich Kahn    | Sruthy Menon  |
| Executive          | Zach Choquette   | Sruthy Menon  |
| Enterprise & Commercial Sales, AMER | Kevin Rodrigues |Sruthy Menon |
| Enterprise Sales, EMEA | Joanna Tourne | Lerato Thipe |
| Customer Success & Solutions Architects, EMEA | Ornella Miles | Lerato Thipe |
| Commercial Sales/R&D, EMEA | Ben Cowdry | Lerato Thipe |
| Global Services & Field Operations | Kelsey Hart  | Fernando Khubeir |
| Customer Success & Solutions Architects, AMER | Barbara Dinoff |  Jazmin Armstrong |
| All Business, APAC | Yas Priatna  | Sruthy Menon |
| G&A/EBA/Marketing (Leadership and E-Group+ EBA) | Steph Sarff | Michelle Jubrey |
| G&A/EBA/Marketing (Global Sales Development, FP&A, EBA) | Caroline Rebello |  Michelle Jubrey |
| G&A/Marketing (Developer Relations, Growth, IT) | Hannah Stewart  | Jazmin Armstrong |
| G&A/Marketing (Corporate Communications, Data, Legal, People, Product Marketing) | Jenna VanZutphen  | Fernando Khubeir |
| G&A/Marketing (Accounting, Integrated Marketing, Internal Audit, Marketing Ops, Tax) | Aiste Juozaponyte  | Fernando Khubeir |
| Engineering, Development | Matt Angell | Alice Crosbie |
| Engineering, Development | Sara Currie, Chux Ugorji | Jazmin Armstrong |
| Engineering, Development | Joe Guiler, Heather Tarver, Seema Anand | Teranay Dixon |
| Engineering, Infrastucture   | Maxx Snow | Teranay Dixon  |
| Engineering, Infrastucture   | Michelle A. Kemp, Aziz Quadri | Alice Crosbie  |
| Customer Support | Joanna Michniewicz  |  Alice Crosbie |
| Product Management | Holly Nesselroad | Fernando Khubeir |
| Security | Holly Nesselroad | Fernando Khubeir |
| Design/UX  | Riley Smith | Lerato Thipe  |
| Emerging Talent  | Justin Smith | Michelle Jubrey  |

For urgent requests of the Candidate Experience Specialist team, we encourage you to contact them by also tagging @CES in Slack messages and CC'ing CES@gitlab.com on emails.

## Talent Acquisition Leader Alignment

| Department                    | Leader      |
|--------------------------|-----------------|
| Talent Acquisition         | Jess Dallmar |
| Talent Brand | Devin Rogozinski |
| Talent Acquisition (Sales) | Jake Foster|
| Talent Acquisition (EMEA and APAC Sales) | Jake Foster |
| Talent Acquisition (Marketing & G&A) | Steph Sarff + Jake Foster |
| Talent Acquisition (G&A) | Steph Sarff + Jake Foster |
| Talent Acquisition (Development) | TBA |
| Talent Acquisition (Specialty Tech): | Jake Foster |
| Talent Acquisition (R&D: Infrastructure) | TBA |
| Talent Acquisition (Executive) | Rich Kahn |
| Enablement | Marissa Farris |
| Candidate Experience | Marissa Farris |

## Talent Acquisition Platform Directly Responsible Individual

| Platform                    | Responsibility        | DRI     |
|--------------------------|-----------------|-----------------|
| Glassdoor | Admin  | Devin Rogozinski |
| Glassdoor | Responding to Reviews  | Devin Rogozinski |
| Glassdoor | Content Management | Devin Rogozinski |
| LinkedIn | Admin - Recruiter  | Devin Rogozinski |
| LinkedIn | Seats | Devin Rogozinski |
| LinkedIn | Media - General | Marketing |
| LinkedIn | Media - Talent Acquisition | Devin Rogozinski |
| LinkedIn | Content Management | Marketing |
| LinkedIn | Content Management - Life at GitLab | Devin Rogozinski |
