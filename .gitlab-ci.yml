# NOTE: Static Sites often want to optimize for fast build and deploy
#       pipeline times, so changes are published as quickly as possible.
#       Therefore, this default config includes optional variables, settings,
#       and caching, which help minimize job run times. For example,
#       disabling support for git LFS and submodules. There are also retry
#       and reliability settings which help prevent false build failures
#       due to occasional infrastructure availability problems. These are
#       all documented inline below, and can be changed or removed as
#       necessary, depending on the requirements for your repo or project.

###################################
#
# GENERAL/DEFAULT CONFIG:
#
###################################

stages:
  - test
  - build
  - deploy
  - notify

include:
  - component: gitlab.com/components/secret-detection/secret-detection@~latest
# disabling danger bot/reviewer roulette until we resolve #238
#  - component: gitlab.com/gitlab-org/components/danger-review/danger-review@~latest

default:
  interruptible: true # All jobs are interruptible by default
  # The following 'retry' configuration settings may help avoid false build failures
  #  during brief problems with CI/CD infrastructure availability
  retry:
    max: 2 # This is confusing but this means "3 runs at max".
    when:
      - unknown_failure
      - api_failure
      - runner_system_failure
      - job_execution_timeout
      - stuck_or_timeout_failure
  tags:
    - saas-linux-medium-amd64

variables:
  # NOTE: The following PERFORMANCE and RELIABILITY variables are optional, but may
  #       improve performance of larger repositories, or improve reliability during
  #       brief problems with CI/CD infrastructure availability

  ### PERFORMANCE ###
  # GIT_* variables to speed up repo cloning/fetching
  GIT_DEPTH: 0
  # Disabling LFS and submodules will speed up jobs, because runners don't have to perform
  # the submodule steps during repo clone/fetch. These settings can be deleted if you are using
  # LFS or submodules.
  GIT_LFS_SKIP_SMUDGE: 1
  GIT_SUBMODULE_STRATEGY: none

  # Speed up artifact compression and upload
  # https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/2684
  FF_USE_FASTZIP: 1
  ARTIFACT_COMPRESSION_LEVEL: fast

  ### RELIABILITY ###
  # Reduce potential of flaky builds via https://docs.gitlab.com/ee/ci/yaml/#job-stages-attempts variables
  GET_SOURCES_ATTEMPTS: 3
  ARTIFACT_DOWNLOAD_ATTEMPTS: 3
  RESTORE_CACHE_ATTEMPTS: 3
  EXECUTOR_JOB_SECTION_ATTEMPTS: 3

  PAGES_PREFIX: ""

.default-branch: &default-branch
  if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $BUILD_AND_TEST_ONLY != "true"

.merge-request-only: &merge-request-only
  if: $CI_PIPELINE_SOURCE == 'merge_request_event'

.merge-request-or-tag: &merge-request-or-tag
  if: '$CI_MERGE_REQUEST_IID || $CI_COMMIT_TAG'

.scheduled-pipeline: &scheduled-pipeline
  if: '$CI_PIPELINE_SOURCE == "schedule" && $SCHEDULED_DEPLOY == "true"'

.build-and-test-only: &build-and-test-only
  if: '$BUILD_AND_TEST_ONLY == "true"'

###################################
#
# BUILD STAGE
#
###################################

build:
  image: registry.gitlab.com/pages/hugo/hugo_extended:0.123.7
  stage: build
  script:
    - echo -e "\e[0Ksection_start:`date +%s`:group1\r\e[0KGroup 1"
    - apk update
    - echo -e "\e[0Ksection_end:`date +%s`:group1\r\e[0K"
    - echo -e "\e[0Ksection_start:`date +%s`:group2\r\e[0KGroup 2"
    - apk upgrade
    - echo -e "\e[0Ksection_end:`date +%s`:group2\r\e[0K"
    - echo -e "\e[0Ksection_start:`date +%s`:group3\r\e[0KGroup 3"
    - apk add go npm yq curl coreutils sed
    - echo -e "\e[0Ksection_end:`date +%s`:group3\r\e[0K"
    - echo -e "\e[0Ksection_start:`date +%s`:group4\r\e[0KGroup 4"
    - ./scripts/sync-data.sh
    - echo -e "\e[0Ksection_end:`date +%s`:group4\r\e[0K"
    - echo -e "\e[0Ksection_start:`date +%s`:group5\r\e[0KGroup 5"
    - npm ci
    - echo -e "\e[0Ksection_end:`date +%s`:group5\r\e[0K"
    - echo -e "\e[0Ksection_start:`date +%s`:group6\r\e[0KGroup 6"
    - hugo --enableGitInfo
    - echo -e "\e[0Ksection_end:`date +%s`:group6\r\e[0K"
    - echo -e "\e[0Ksection_start:`date +%s`:group7\r\e[0KGroup 7"
    - find "public" -type f -name '*.html' -exec sed -i -e "s/content-sites\/handbook\/${PAGES_PREFIX}\/content-sites\/handbook\/${PAGES_PREFIX}/content-sites\/handbook\/${PAGES_PREFIX}/g" {} +
    - echo -e "\e[0Ksection_end:`date +%s`:group7\r\e[0K"
  artifacts:
    paths:
      - public
  rules:
  - <<: *default-branch
  - <<: *build-and-test-only
  - <<: *merge-request-only
    variables:
      PAGES_PREFIX: 'mr$CI_MERGE_REQUEST_IID'
      ENVIRONMENT: 'mr$CI_MERGE_REQUEST_IID'
      HUGO_BASEURL: "${CI_PAGES_URL}/${PAGES_PREFIX}"  

###################################
#
# DEPLOY STAGE
#
###################################

pages:
  stage: deploy
  environment:
    # tracking DORA deployment frequency
    name: $ENVIRONMENT
    deployment_tier: production
    url: $HUGO_BASEURL
  variables:
    ENVIRONMENT: production
    HUGO_BASEURL: https://handbook.gitlab.com/
  pages:
    path_prefix: "$PAGES_PREFIX"
  artifacts:
    paths:
      - public    
  script:
    - echo "Pages accessible through ${CI_PAGES_URL}/${PAGES_PREFIX}"    
  rules:
    - <<: *default-branch
    - <<: *scheduled-pipeline
    # 2024-05-21 - Temporarily disabled due to > 500 pages deployments blocking
    #- <<: *merge-request-only
    #  variables:
    #    PAGES_PREFIX: 'mr$CI_MERGE_REQUEST_IID'
    #    ENVIRONMENT: 'mr$CI_MERGE_REQUEST_IID'
    #    HUGO_BASEURL: "${CI_PAGES_URL}/${PAGES_PREFIX}"

####################################
##
## TEST STAGE
##
####################################

markdownlint:
  image:
    name: davidanson/markdownlint-cli2:v0.12.1
    entrypoint: ["/bin/sh", "-c"]
    docker:
      user: root
  stage: build
  needs: []
  variables:
    FORCE_COLOR: 1
  before_script:
    - npm install chalk terminal-link
  script:
    - apk add git
    - sh ./scripts/markdownlint.sh
  rules:
    - <<: *merge-request-or-tag
    - <<: *build-and-test-only
  artifacts:
    when: always
    expire_in: 1 month
    paths:
      - markdownlint-cli2-codequality.json
    reports:
      codequality: markdownlint-cli2-codequality.json

handbooklint:
  image:
    name: gitlab/glab
    entrypoint: ["/bin/sh", "-c"]
  stage: build
  needs: []
  before_script:
    - apk update
    - apk upgrade
    - apk add jq sed bash
  script:
    - bash ./scripts/handbook-lint.sh
  rules:
    - <<: *merge-request-or-tag
    - <<: *build-and-test-only
  artifacts:
    when: always
    expire_in: 1 month
    paths:
      - handbook-codequality.json
    reports:
      codequality: handbook-codequality.json

hugolint:
  stage: build
  needs: []
  rules:
    - <<: *build-and-test-only
    - <<: *default-branch
      allow_failure: true
    - <<: *merge-request-or-tag
      allow_failure: true
  image:
    name: registry.gitlab.com/gitlab-com/content-sites/handbook-tools/hugolint:latest
  script:
    - hugolint linkcheck --format=json $(find content -type f -name '*.md' | grep -v ' ') >linkcheck.json
    - hugolint missinglinks --num 30 $(find content -name '*.md' | grep -v ' ') >missinglinks.txt 2>/dev/null
    - head -n 20 missinglinks.txt
  artifacts:
    when: always
    expire_in: 1 month
    paths:
      - linkcheck.json
      - missinglinks.txt
    reports:
      codequality: linkcheck.json

secret_detection:
  stage: build
  needs: []
  dependencies: [] # Don't download artifacts, especially `./public/`
  rules:
    - <<: *build-and-test-only
    - <<: *merge-request-or-tag

####################################
##
## NOTIFY STAGE
##
####################################

post_comment_on_failure:
  needs: ["markdownlint", "handbooklint"]
  when: on_failure
  stage: notify
  image:
    name: gitlab/glab
    entrypoint: ["/bin/sh", "-c"]
  script:
    - apk add yq jq
    - ./scripts/parse-codequality-report.sh -r ./codequality.json > msg.txt
    - glab auth login -t $MR_UPDATE_TOKEN
    - glab mr note --unique $CI_MERGE_REQUEST_IID -m "$(cat msg.txt)"
  rules:
  - <<: *merge-request-or-tag

notify_slack_on_build_failures:
  when: on_failure
  stage: notify
  script: ./scripts/send-failure-notification-to-slack.sh
  rules:
  - <<: *default-branch
